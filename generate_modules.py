import yaml

def generate_module_list():
    with open('/tmp/modules.yml', 'r') as file:
        data = yaml.safe_load(file)
        modules = [f"{data['modules'][module]['repo']}#{data['modules'][module]['tag']}#{data['modules'][module]['folder_name']}" for module in data['modules']]
        return modules

def write_module_list_to_file(modules):
    with open('/tmp/modules_to_install.txt', 'w') as output_file:
        output_file.write('\n'.join(modules))

if __name__ == "__main__":
    modules = generate_module_list()
    write_module_list_to_file(modules)
