#!/bin/bash

# Define the input file
MODULES_FILE="/tmp/modules_to_install.txt"

# Check if the file exists
if [ ! -f "$MODULES_FILE" ]; then
    echo "Error: $MODULES_FILE not found."
    exit 1
fi

echo "EPICS_BASE=/opt/epics/base" > "RELEASE.local"
echo "SUPPORT=/opt/epics/support" >> "RELEASE.local"

while IFS='#' read -r repo_url tag module_name || [[ -n "$repo_url" ]]; do
    if [ -z "$repo_url" ]; then
        continue
    fi

    echo "Installing $module_name from $repo_url with tag $tag"

    git clone --depth 1 --recursive --branch "$tag" "$repo_url" "${SUPPORT}/${module_name}"
    echo "$(echo "${module_name}" | tr '[:lower:]' '[:upper:]')=${SUPPORT}/${module_name}" >> "RELEASE.local"
    make -C ${SUPPORT}/${module_name} -j $(nproc)
done < "$MODULES_FILE"

sed -i 's/\<SEQ\>/SNCSEQ/' "RELEASE.local"

cp RELEASE.local /opt/epics/RELEASE.local
cp RELEASE.local ${SUPPORT}/RELEASE.local
