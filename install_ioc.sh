#!/bin/bash

REMOTE_YAML_PATH="modules.yml"  # Path to the YAML file on the remote server

# Function to extract module values using yq
extract_module_values() {
    local module="$1"
    yq e ".ioc.folder_name + \",\" + .ioc.source + \",\" + .ioc.tag + \",\" + .ioc.user" "$REMOTE_YAML_PATH"
#+ .modules.${module}.tag + \",\" + .modules.${module}.folder_name" "$REMOTE_YAML_PATH"
}

# Fetch values for specific modules from the YAML file using yq
IOC_INFO=$(extract_module_values "ioc")

echo $IOC_INFO
# Assign extracted YAML values to Bash variables
IFS=',' read -r IOC_FOLDER_NAME IOC_REPO IOC_TAG IOC_USER <<< "$IOC_INFO"

echo "IOC_FOLDER_NAME: $IOC_FOLDER_NAME, IOC_REPO: $IOC_REPO"
echo "IOC_TAG: $IOC_TAG, IOC_USER: $IOC_USER"

#create testIoc directory and go inside it
mkdir $IOC/$IOC_FOLDER_NAME 
cd $IOC/$IOC_FOLDER_NAME
makeBaseApp.pl -u $IOC_USER -t example $IOC_FOLDER_NAME
makeBaseApp.pl -u $IOC_USER -i -p $IOC_FOLDER_NAME -t example $IOC_FOLDER_NAME
make

# retrieve source files
git clone --depth 1 --recursive --branch "$IOC_TAG" "$IOC_REPO" "$IOC/source"

#copy source files ending with .cmd to folder iocBoot/iocTest
cp $IOC/source/*.cmd $IOC/$IOC_FOLDER_NAME/iocBoot/ioctestIoc/
# make st.cmd executable
chmod +x $IOC/$IOC_FOLDER_NAME/iocBoot/ioctestIoc/st.cmd
# copy source files ending with .db to folder db
cp $IOC/source/*.db $IOC/$IOC_FOLDER_NAME/db/